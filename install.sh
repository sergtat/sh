#!/bin/bash

mkdir -p ~/.sh/zkbd
touch ~/.sh/bashrc.local
touch ~/.sh/zshrc.local
ln -sf .sh/zkbd ../.zkbd
ln -sf .sh/bashrc ../.bashrc
ln -sf .sh/bash_logout ../.bash_logout
ln -sf .sh/bash_profile ../.bash_profile
ln -sf .sh/bashrc.local  ../.bashrc.local
ln -sf .sh/profile ../.profile
ln -sf .sh/env ../.env
ln -sf .sh/zlogin ../.zlogin
ln -sf .sh/zlogout ../.zlogout
ln -sf .sh/zprofile ../.zprofile
ln -sf .sh/zshenv ../.zshenv
ln -sf .sh/zshrc ../.zshrc
ln -sf .sh/zshrc.local  ../.zshrc.local
